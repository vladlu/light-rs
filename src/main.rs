use std::io::Write;
enum Operation {
    Inc(u8),
    Dec(u8),
}

const BACKLIGHTMIN :u8 = 5;

fn main() -> std::io::Result<()>{
    let cmdargs :Vec<String> = std::env::args().into_iter().collect::<Vec<String>>()[1..].to_vec();
    let op = match cmdargs[0].as_str() {
        "inc" => match &cmdargs[1].parse::<u8>() {
            Ok(num) => Operation::Inc(*num),
            Err(_) => std::process::exit(1),
        }
        "dec" => match &cmdargs[1].parse::<u8>() {
            Ok(num) => Operation::Dec(*num),
            Err(_) => std::process::exit(1),
        }
        _ => std::process::exit(1)
    };
    let backlight_max = match std::fs::read_to_string("/sys/class/backlight/amdgpu_bl0/max_brightness").unwrap_or("0".to_string()).replace("\n", "").parse::<u8>() {
        Ok(o) => o,
        _ => {
            eprintln!("Insuficient Priv");
            std::process::exit(1);
        }
    };
    let backlight_val = match std::fs::read_to_string("/sys/class/backlight/amdgpu_bl0/brightness").unwrap_or("0".to_string()).replace("\n", "").parse::<u8>() {
        Ok(o) => {
            o
        }
        _ => {
            eprintln!("Error2");
            std::process::exit(0)
        }
    };
    match op {
        Operation::Inc(a) => {
            let new_val = 1.0 + backlight_val as f32 + backlight_val as f32 * a as f32 / 100.0;
            if (new_val < BACKLIGHTMIN as f32) | (new_val > backlight_max as f32) {
                std::process::exit(1);
            }
            let new_val = format!("{}", f32::ceil(new_val));
            let mut back = std::fs::OpenOptions::new().write(true).truncate(true).open("/sys/class/backlight/amdgpu_bl0/brightness")?;
            println!("{}", new_val);
            back.write_all(new_val.as_bytes())?;
            back.flush()?;
            Ok(())
        }
        Operation::Dec(a) => {
            let new_val = -1.0 +backlight_val as f32 - backlight_val as f32 * a as f32 / 100.0;
            if (new_val < BACKLIGHTMIN as f32) | (new_val > backlight_max as f32) {
                std::process::exit(1);
            }
            let new_val = format!("{}", f32::floor(new_val));
            let mut back = std::fs::OpenOptions::new().write(true).truncate(true).open("/sys/class/backlight/amdgpu_bl0/brightness")?;
            println!("{}", new_val);
            back.write_all(new_val.as_bytes())?;
            back.flush()?;
            Ok(())
        }
    }

}
